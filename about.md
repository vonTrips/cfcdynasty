# Football Coach: College Dynasty návod (recenze)
Takový malý návod jako úvod do hry Football Coach: College Dynasty. Což je relativně jednoduchý manažer amerického fotbalu, který vychází z původní hry Football Coach 2 (Android, iOS). Hráč se ujímá vedení nějaké univerzity (mód Škola) nebo jako HC vede nějaký program (mód Kariéra).

Důležité je hned na začátku říct, že se nejedná o žádnou licencovanou hru! Proto v originálu nenajdete oficiální rozdělení konferencí, oficiální školy ani nic podobného. Výchozí nastavení herního světa je zjednodušení FBS o 6 konferencích a celkem 78 týmech. Pokud si chcete hru opravdu užít, je třeba stáhnout a pro vytvoření hry využít některý z uživatelských nastavení (universe, formát JSON). Ty jsou aktuálne dostupné hlavně na Discordu, časem snad i přes Steam Workshop. Popř. zde v repositáři v [adresáři universe](universe).

Mj. toto je i důvod proč Šráma na jednu mou otázku odpověděl, že hru (mobilní verzi, ale CFC) zkusil a po minutě opustil. Bez uživatelských souborů (pro mobilní hry existují i soupisky hráčů a trenérů) to fakt není ono.

## Custom Universe (uživatelské nastavení herního světa)
U této položky se ještě zastavím a to kvůli hře samotné. Hru dělá z hlavní části jediný člověk, který tak činí ve svém volnu (není to jeho práce). Proto jsou některé věci jednodušší/zjednodušené a nemusí úplně odpovídat realitě. Spousta věcí se však postupně mění a změny jsou od prvních vydaných verzí opravdu markantní.

Bohužel se to zjednodušení týká i rozdělení nejvyšší divize na konference. Hra zvládne pracovat se 6, 8 nebo 10 konferencemi. Každá z nich může mít jednu divizi po 10 týmech, dvě divize po 6 nebo 7 týmech, popř. 4 divize po 16 týmech. Z toho jasně vyplývá, že minimum týmů na jednu hru je 60 a maximum 160. Nevýhodou je, že neexistují (zatím) nezávislé týmy mimo konference. Každý tým musí být v nějaké konferenci (tedy i např. Notre Dame). Pořád se však hraje na sílu konferencí, takže pokud je konferencí víc jak 6, existuje tzv. Power5.

Toto rozdělení pak řeší právě ony uživatelské soubory (custom universe). Některé jsou povedenější, jiné méně. V adresáři jsou primárně 3:
 * **Jeff mod FBS** - aktuálně nejlepší soubor pro FBS; 10 konferencí, celkem 134 týmů (nejslabší konferencí je C-USA a týmy v ní, Jacksonville State a Kenshaw)
 * **Quintos FCS** - soubor, který řeší herní svět pro FCS (nezkoušel jsem);
 * **D2 universe** - soubor, který do hry dostane Divizi 2 (nezkoušel jsem);

Použití jednoho z těchto souborů (doporučuji začít s Jeffovým FBS) je víceméně podmínkou, abyste si hru užili. V tomto souboru jsou správně pojmenovány všechny Bowly, nastaveny konference, přiřazené týmy, rivality a spousta dalších věcí. Dokonce si autor pohrál s výzkumem z posledních 10 a více let, takže docela dobře sedí prestiž programu, zázamí nebo fanoušci. Kdo chce, může si více přečíst na Discordu hry, autor tam docela podrobně popisuje každé vylepšení svého souboru.

## Nastavení samotné hry
Při vytváření hry jsem tedy zvolil Jeff mod FBS a nastartoval jsem novou hru (soubor stačí stáhnout kamkoli na disk). Možností nastavení hry je víc a zásadně ovlivňují chování celé hry. V průběhu hry nejde nic z toho změnit (leda manuální úpravou JSON souboru hry, ale to je bez záruky - já to nezkoušel).

![Nastaveni hry](/manual/1-nastaveni.PNG)

### New Game Settings
Tady je možné nastavit šest důležitých aspektů hry:
 * **Playoff format** - výchozí je 12členný formát s 6 vítězi konferencí a 6 pozvánkami; ale je možné zvolit cokoli od Single Game po 16 týmů;
 * **Draft Eligibility** - výchozí je 3 roky (jako v realitě);
 * **NIL** - asi největší zlo aktuálního univerzitního fotbalu je možné vypnout! Výchozí stav je však zapnuto se stropem na 21M;
 * **Injury Frequency** -  jak často budou zranění od reálného (výchozí) po vypnuto (nuda);
 * **Transfer Eligibility** - kdo vše může na TP? Výchozí stav je bez restrikcí, ale je možné TP i vypnout;
 * **Conference Realignment** - jak často bude docházet ke změně struktury konferencí a k přesunu týmů mezi nimi; výchozí stav je 10 let, ale je možné přeskupení vypnout;

Vše je ve hře dobře popsáno. Ve zkratce lze říct, že by neměl být problém nastavit si herní svět jako např. v 50. letech. Soubory s nastavením takového světa by měly být časem k dispozici a dokonce by měla existovat i podpora ve hře (např. omezené playbooky, extrémní upřednostnění běhů přes passy ap.). Nebo je možné se vrátit zpět jen pár let, kdy se ani nehrálo Play-off, neexistoval NIL a byl podstatně omezený Transfer portal.

Osobně jsem nastavil TP na minimálně 2 roky, neboť mi to přijde smysluplnější. A upravil jsem si pro vlastní potřeby přeskupení. Začal jsem totiž hru za jednu z nejhorších univerzit (Jacksonville State) a nechce se mi čekat, jestli po 10 letech "postoupíme" do lepší konference nebo ne. Takže jsem si tam dal nejnižší hodnotu - 5 let.

### Herní mód
Na výběr jsou dva módy a každý pak dává jiné možnosti dalšího nastavení:
 * **Školní mód** - hrajete celou dobu kariéru za jedinou školu (de facto se stáváte sportovním ředitelem);
 * **Trenérský mód** - stáváte se 30letým HC na zvolené univerzitě a máte 50 sezón na úžasnou kariéru, pak konec;
 * **Koordinátor mód** - časem by do hry měl přibýt ještě mód koordinátora, kdy ve 30 letech začnete jako OC/DC;

#### Trenérský mód
Ještě jsem jej nehrál, takže jen ve zkratce. Nastavit si můžete prakticky cokoli (ksicht, vlasy, jméno atd.), ale hlavně jde o to co budete za archetyp. Archetyp totiž určuje skilly vašeho alter ega, každý trenér má totiž 1-10 u čtyř kateogirí - útok, obrana, trénování a nábor. Každý má vliv na jinou část hry. Osobně doporučuji "Pure CEO", sice máte útok a obranu jen 1, ale trénování a nábor 4. V trenérském módu se počítá, že budete volat vlastní akce, proto není zapotřebí mít vysoké úrovně útoku a obrany (o to se postarají OC a DC).

Dále nastavujete preferované ofenzivní a defenzivní schéma. V ofenzívě jsou 3 playbooky (Balanced, Spread a Pro style) a 7 různých stylů playcallu (od Heavy run, přes Balanced, po Heavy pass). V obraně jsou playbooky 4 (4-4 Split, 4-3, 3-4 a Nickel), u všech je pak možné nastavit styl Playcallu na Man/Zone a opět ve variantách (Agresive - Balanced - Conservative). Volba schémat/playbooků je velmi důležitá!

Při zakládání hry to ovlivňuje jaké OC/DC budete mít k dispozici (lze si vybrat), ale hlavně to má zásadní vliv na počet akcí a hlavně jejich typ, které bude možné při hraní použít. Při Spread/Pass first mi v playbooku chyběly mnohé běhové hry, které bych rád volal, ale prostě to nešlo.

Co se týče koordinátorů, vždy je lepší zvolit horšího (skilly) OC/DC, který však odpovídá schématu HC než naopak. Ve hře je totiž postih (primárně pro simulace) za to, že HC a jeho koordinátoři hrají jiné schéma. Na začátku sezóny se totiž stejně musí nějaký playbook vybrat a mít na výběr z více není nejlepší varianta. Na závěr už si jen vyberete ve které škole chcete začít. Ale pozor, pokud máte AD (Atletic Director) horkou hlavu, stačí abyste vyhráli o jediný zápas méně a dostanete padáka. Kariérní (trenérský) mód má tu nevýhodu, že když výsledky nejsou, jdete na dlažbu. Naopak, když budou výsledky nadprůměrné, může Vám hodit laso lepší program.

#### Školní mód
 V tuto chvíli můj oblíbený, protože i ve Football Manageru raději buduji kluby od začátku, klidně z pralesa, směrem k úspěchům - více mě to baví. Proto jsem zatím zkoušel tento mód. V nastavení má zásadní nevýhodu, nevybíráte si žádné lidi, ty Vám prostě hra vygeneruje a basta. Takže Vás pak čeká pár sezón, kdy budete postupně budovat kádr, ale i realizační tým (je třeba si dát pozor, že za propuštění HC/OC/DC dochází k penalizacím).

Nastavit tak je možné pouze školu, kterou chcete ovládat a zvolit si hlavního rivala (zatím může být jen jeden). Ale je možné si zvolit školu (třeba nějakou slabou v nejhorší konferenci) a místo ní si vytvořit vlastní. Stejně tak je možné vytvořit této "fiktivní" škole i fiktivního rivala, vše záleží jen na nás.

Jednou z kaněk tohoto módu je však aktivní AD ve hře. Celý mód totiž působí právě spíše jako AD mód, kdy jste "neomezeným" vládcem celého programu. Ale vlastní postavu (AD) tam bohužel nevytvoříte, což je škoda. Na druhou stranu asi i díky tomu je možné v tomto módu odehrát více než 50 sezón. 

## Nová hra
Řekněme, že začneme školní kariéru za Stanford (tahle škola mě vždy fascinovala, že i když hraje v D1-FBS, tak dbá na skvělé studijní výsledky svých hráčů). Samotná škola má prestiž 5 (nejlepší ve hře je 10) a kvalitu 40 (stadion-5, fanbase-4, facilities-7, college life-9, marketing-10 a academics-9). Toto nejsou vůbec špatné hodnoty, hlavně kvalita zázemí je super. Nejlépe je na tom Michigan (48) a Texas (46). Ani prestiže Stanfordu není k zahození, 5 je pořád dobré - dává to kvalitní výchozí pozici. Nejlepší je v prestiži Alabama (10).

![Volba hry](/manual/2-nova_hra.PNG)

Vše vychází z rozsáhlého průzkumu autora tohoto nastavení, lze se setkat i s jinými. Dále víme, že hraje v konferenci PAC-10 a hlavním rivalem je California (Univerzita z Berkeley). Stačí stisknout BEGIN GAME a začne se generovat hra - 134 týmů a více než 8000 hráčů. Záleží na síle počitače, ale chvilku to trvá. I na mém Ryzen 5700X s 32 GB RAM to bylo více než 10 sekund.

### Stručný popis
Samotná hra se skládá ze tří hlavních částí - Preseason, Season a Offseason. Podrobně jsou jednotlivé části popsány tady i s dalšími radami https://gitlab.com/vonTrips/cfcdynasty/-/blob/main/manual/manual-stages.md ale časem z toho chci udělat Wiki v tomto repositáři, aby se tam dala lépe udělat interaktivita mezi stránkami.

Každá hlavní část je pak rozdělena na několik fází, kdy se v každé fázi dělá něco jiného. Někdy to je jediná činnost (změna pozic u hráčů), jindy je těch činností víc (trénink, tréninkové hry, nábor).

Nejpomalejší je určitě Preseason, kde je lepší nepřeskočit žádnou fázi ani dílčí činnost. Nastavit nové pozice je rychlovka, shlédnout změny v tréninku také. Pokud už máte stabilní kádr a stabilizovanou prestiž, na vrcholu tréninku by se měli objevit primárně Sernior hráči:

![Mezisezonni trenink](/manual/3-trenink.PNG)

Občas není špatné se podívat, kdo se jak zlepšil a jestli zlepšení odpovídá potenciálu, popř. jestli nedošlo k jeho změně. Ale důležitější je "Game Plan Install", tedy nastavení zápasových plánů. Playbook se volí pouze před sezónou a vždy je na celý rok. Měnit se může jen styl, na to je třeba si dát pozor. Doporučuji věnovat pozornost nastavení rotace u hráčů, jestli mají hrát více starteři nebo má být hlubší rotace. Závisí to na kvalitě hráčů, doporučuji projít Depth Chart a vše důsledně nastavit. 

Dobrým freshmanům, kteří ale nejsou starteři, doporučuji rovnou nastavit RedShirt - rok v posilovně a na tréninkovém hrišti se jim bude hodit. Zase je třeba si ale dát pozor na potenciál. Nemá cenu nechat jenom trénovat hráče s potenciálem F, ten se stejně moc nezlepší a pravděpodobně bude RS automaticky.

Co se týče Roosteru, např. uživatel Swiss na Steamu psal, že Starter by měl mít vždy OVR alespoň 80 a víc. A ideální ofenzivní a defenzivní talent je také 80+, pak se dají dělat docela solidní výsledky. Já měl ale v Jacksonville v první sezóně pouze 72 a 71, ale přesto jsem vyhrál konverenci USA (bylo to však díky tomu, že jsem sám calloval hry).

### Různé tipy
Další fáze (Spring 1 a dál) jsou popsány v připojeném dokumentu, tady už se dále vypisovat nebudu. Ale sepíšu pár tipů, které se mohou při hraní hodit.

Co se týče trenérů HC/OC/DC, je jedno jestli hrajete školní nebo kariérní mód, vždy závisí na tom, jestli chcete volat vlastní hry! Pokud ano, není třeba najímat trenéry s vysokým hodnocením ofenzívy nebo defenzívy (to ovlivňuje "čtení" soupeře a nastavení her). Jako uživatel (hráč) nemůžete ovlivnit trénink a nábor, takže je lepší vzít hlavně trenéry co mají nejvyšší možné hodnocení trénování a náboru (obojí docela dost výrazně pomůže).

Já třeba neřešil defenzívu, moc dobře se tam necítím, takže obranné hry volal DC. U něj jsem proto chtěl vysokou úroveň defenzívy (měl 7, když začínal) a omeželil jsem horší trénink a nábor. Ale u HC jsem byl rád, že nebyl úplně zabraný do ofenzívy a měl solidní nábor i trénink (dokonce +1 akci). Ovšem OC, kterého jsem musel najmout, jsem bral vyloženě podle atributů náboru a tréninku. Věděl jsem, že akce chci volat sám a lépe využiju toho, že budu mít třeba jednu další tréninkovou akci navíc.

To samé platí i u zlepšování trenérů na konci sezóny - to totiž určujete Vy jakožto uživatel hry. Já u DC primárně vylepšoval defenzivní schopnosti a pak ten zbytek. U HC (školní mod) to naopak byl trénink a nábor, což ale doporučuji i u kariérní verze hry. U OC pak záleží na tom, jestli chcete volat hry sami, nebo to necháte na počítači.

Někdy v manuálech je to popsáno, ale není od věci si projít (někdy po 4. týdnu) všechny hráče v náboru, kteří stále teprve zjišťují své schopnosti (Exploring options). Někteří z nich (zpravidla 1* a 2*) u sebe nemají žádný zájem, takže je možné získat solidního hráče de facto zadarmo (stačí jej jen zacílit, cca +100 bodů každý týden). Po skautingu můžete klidně nalézt hráče co má OVR 65+ a potenciál i výdrž B a lepší.
# Jak hrát hru různými styly

## Heavy run (běh na prvním místě)
OL musí být TOP v "run block" a QB musí být běhoví.
Najít OC, který má "Run Blocking +2 skill" a poté "Ball security"
Při playcallingu se snažit o překvapení (běžet na 1&10 nebo 2&8, ale pass na 2&2 ap.)
Otestovat si všechny běhy proti základní obraně a zjistit které fungují, kdy fungují a které nefungují vůbec (Bubble)

Je to nejtěžší způsob hry, protože hra je spíše passová

## Spread offense (prostě všechno vrchem)
# Jednotlivé části

Celá hra je rozdělena na několik částí, zde jsou všechny popsány:
* Preseason - předsezónní období je hlavně o tréninku a vytipování cílů pro rekrutování
* Season - hlavní část, kde se odehrává příprava na utkání a samotné utkání; v případě dobrých výsledků i bowly nebo play-off
* Offseason - zlepšeování a podpisy trenérů, podpisy rekrutů, transfer portal a další posezónní detaily (změny v konferencích, boosterech atd.)


## Preseason
Na začátku se prochází částmi, které jsou poměrně rychlé, ale mohou znamenat mnoho. Začíná se první částí, kde je možné pozměnit hráčům jejich role a zaměření. Toto je paradoxně důležitá část, kterou není radno přeskočit. Hra má doporučení udržovat kádr o velikosti 61 hráčů (rozdělení je v manuálu k náboru), jenže ne vždy se podaří rekrutovat dostatek hráčů na danou pozici (třeba DL nebo LB bývají problém). Takže není od věci nabrat některých hráčů víc (např. atletické Safety) a v této první části jim změnit přidělenou roli (z atletického Safety udělat LB). V úplně první sezóně je možné tuto část ignorovat, později už nikoli.

Následuje část, která je čistě informativní - výsledky tréninkového procesu za uplynulou sezóny (Training Results). Poté už přichází na řadu nastavení plánu pro danou sezónu - kdo má hlavní slovo, podle čeho se jede atd. To má vliv na kvalitu volaných her (což může být jedno, pokud hry voláte sami, viz níže), ale hlavně to má vliv na počet a typ akcí, které je možné volat. Zároveň se zde nastavují rotace pro jednotlivé hráče a je možné nastavit hráčům i RS.

Po těchto třech částech následují ty nejdůležitější, tréninkové. Jsou rozděleny na čtyři samostatné části jarního (2x) a letního (2x) tréninku a v každé části se dá pracovat s tréninkem hráčů (jejich zlepšování pomocí akcí HC/DC/OC), s tréninkem akcí (jen pro seznámení, co jak funguje) a rekrutování (postupné akce pro nábor hráčů - začíná se na jaře).

### Trénink hráčů (training)
Je možné vše nechat na automatice, ale pak se může stát, že se Vámi preferovaný hráč nezlepší dostatečně. Popř. se nezlepší vůbec. Proto je vhodnější vytipovat startery, popř. RS hráče a těm umožnit v každé části zlepšit nějakou slabinu jejich hry. Vše musí být založeno na systému. Takže pokud chci hrát Spread, potřebuji QB, který dokáže házet (Accurancy, Strenght), ale zároveň se dokáže i vyhnout tlaku (Evasion) atd. 

Úroveň zlepšení je dána hodně o náhodě (někdy to může být +1 až +10, jindy jen +1 až +4), ale zároveň i o kvalitě trenérů a zázemí univerziy (Facilities). V rámci tréninku hráčů je možné rozdělit určitý počet akcí (záleží na kvalitě trenérů), tzn. že je možné zlepšit úroveň u stejného počtu hráčů. Vyplatí se investovat primárně do starterů, do hráčů s dobrým potenciálem (B a lepší) a hlavně do mladších hráčů (u Sr už může být trénink jen zbytečným plýtváním). Osobně považuji využití tréninkových akcí u Seniorů za plýtvání. U TOP hráčů, kteří by mohli jít na draft už jako junioři, je třeba si dobře rozmyslet, jestli na ně trénink využít.

Nevím jestli je to tak i myšleno, ale vyšších zlepšení jsem téměř vždy dosáhl u hráčů s lepším potenciálem. Samozřejmě i hráč s potenciálem A si může připsat pouze +1, ale u hráče s potenciálem F jsem nikdy nezískal +6 a víc, i když jsem třeba zlepšoval vlastnost, kde zlepšení mělo být 1-10. 

### Trénink akcí (practice)
Zde je vhodné trénovat akce, které sedí do systému, kterým má tým hrát. A volit primárně základní obrany. Vše slouží ke zjištění, které akce z celého balíku mají šanci na úspěch, kdy a jak. Při Spread offense systému nemá cenu trénovat běhy se 2 RB ap., a některé formace/hry ani nejsou k dispozici. Já jsem Spread hrál první sezónu, když jsem měl Spread OC, ale HC byl Balanced. Když po první sezóně OC odešel a najal jsem nového (Balanced), playbook se výrazně proměnil.

Před samotným tréninkem jednotlivých akcí doporučuji projít rozpis utkání (Team board > Schedule) a zjistit gameplan všech soupeřů. Zpravidla se v tom dá najít souvislost, takže je možné trénovat akce proti konkrétní obraně. Např. v první sezóně za Jacksonville jsem hrál 6x proti 4-4 obraně, a 7x proti nějaké formě zónové obrany.

### Rekrutování (recruiting)
Systémů a doporučení je mnoho. Někdo říká nejdříve vyplácat skauting, pak teprve dělat zacílení (Target). Vždy je lepší nabrat hráče co má menší OVR (Overall Rating), ale vyšší potenciál (A), než brát hráče s vyšším hodnocením (OVR), ale s nízkým potenciálem (D a méně). Problém rekrutování je, že zpočátku o hráčích nevíte mnoho a máte jen 10 skautských akcí (na zjištění potenciálu potřebujete 2). Takže stejně pak musíte zacílit na hráče bez hlubší znalosti. 

Základem je hvězdičkové hodnocení (takoví hráči mají být lepší i do budoucna), celkové hodnocení OVR (v průběhu skautování se hodnota může zásadní změnit) a pak hlavně body zájmu, které u hráče máte po initial contact. Pokud nejste Alabama, nemá cenu cílit na hráče, kde bychom ztráceli už od zaáčtku příliš mnoho bodů. A zároveň je třeba se dívat na TOP priority hráče. Jestliže nemáme zájem dávat hráčům peníze, není třeba vůbec uvažovat nad těmi, kteří mají NIL v TOP3 prioritách. Totéž platí o hráčích, kteří chtějí zůsata blízko domovu (pak hráče z Californie na Floridu neodstanete), popř. chtějí výborný studentský život nebo skvělé studijní podmínky (tito hráči jsou nejlepší, zpravidla bývají loajální). 

Na druhou stranu, pokud trénujete velmi slabou školu, může být právě NIL a herní vytížení tím, co daného hráče do školy přiláká. Což je mj. důvod proč jsem si NIL zatím v žádné hře nevypl. V reálu bych asi raději viděl univerzitní fotbal bez NIL, nebo s velmi omezeným systémem, jenže ve hře mi to umožní posílit i tak slabou univerzitu jako je právě Jacksonville.

#### Rekrutovací akce
Každý tým má 12 základních akcí, které má k dispozici po celou sezónu od Spring 1 až do týdne s konferenčním finále. 6 akcí má HC, po třech oba koordinátoři. Kvalita náborových akcí (tzn. kolik bodů zájmu si u rekruta připíše Váš tým) je dána kvalitou trenéra a prestiží školy.

V rámci preseason je pak ještě 10 speciálních akcí každý tréninkovýc týden - Camp Visit. Tyto akce přidávají méně bodů (např. než HC ACE), ale umožní to rekrutovi poznat univerzitu a hlavně je to dalších 10 akcí, které je možné udat. Pak existují ještě akce nabídky stipendií a NIL, ale tyto rozhodně nepoužívat v rámci Preseason.

#### Oficiální návštěvy
Tyto je třeba neplést s Camp Visit, jsou to dvě zcela nezávislé věci. Někteří rekruti umožní naplánovat oficiální návštěvy už druhý tréninkový týden, ale nedá se z toho úplně usuzovat, jestli se chtějí i dříve zavázat. Plán oficiálních návštěv je důležitý, pokud to uděláte příliš brzy, ostatní zájemci pozvou rekruta později (za každou další návštěvu je +100 bodů, u 4. školy je to +400). Pokud příliš pozdě, rekrut se může zavázat jiné škole, aniž by k Vám přišel.

Toto je velmi ošidné a je třeba si na to dát pozor. Na dřívější termíny zvát hráče, které sice cílíte, ale nejsou pro Vás prioritou. Na poslední týdny (zvláště rivalitní zápas) pozvat TOP rekruty, kteří ale co nejdéle uvažují nad svými možnostmi. Není nic horšího, než TOP rekrut, který je pozván na Week13, ale už ve W4 je ve stavu "Will commit soon".

#### Stipendia a NIL
Důležitá poznámka hned zde, nenabízejte stipendia ani NIL dokud to není nutné!!! Hráči se dlouho rozvažují, takže dokud nejsou ve stavu "Will commit soon" nenabízejte ani stipendia, ani NIL. Nabídnuté stipendia se totiž těžko berou zpět (dříve to nešlo, nyní pouze za ztrátu integrity). A pokud si vyplýtváte stipendia a NIL na rekruty v průběhu sezóny, nezbyde Vám nic na Transfer portal, kde se dají sehnat mnohdy lepší a hlavně ověření hráči, resp. rovnou starteři.

**Takže dokud se hráč nechystá zavázat, nenabízet stipendia ani NIL.**

[Více o rekrutování v samostatném manuálu.](/manual/manual-recruitment.md)

## Sezóna
Rozdělena na týdny, kdy v každém týdnu je možné pořád pracovat na rekrutování - podávat nabídky stipendií, popř. NIL, skautovat hráče nebo si je dokonce přidat na seznam cílů.

### Plán na zápas (Game plan)
Playbook je daný, ale je možné upravovat Playcall style. Pokud se však neshoduje s preferencemi HC ani OC/DC, získávají se nějaké penalizace (ještě nevím jaké). Toto se dá celkem eliminovat, pokud si každou akci budete volat sami. Což je mj. možné jak ve školním, tak v kariérním módu.

### Rekrutování (recruiting)
Pokračuje se ve snaze podepsat vytoužené cíle. Je třeba něco investovat (NIL, stipendia, skautování) a hlavně naplánovat návštěvy. Zároveň je vhodné vzdát se cílů, které jsou v danou chvíli již nereálné pro podpis (např. pokud ztrácím 1000 a více bodů, už bych toho hráče neměl řešit).

I to má však své meze. Podařilo se mi ve třetí sezóně přetlačit Auburn u 4* DL, když jsem mu nabídl 500k v NIL a měl jsem jej pozvaného na rivalitní zápas. Byla to 4. návštěva, takže +400 bodů. Hráč byl už od W7 "Will commit soon", ale když jsem mu nasypal víc než Auburn, vydržel do 13. týdne a díky skvělému zápasu jsme si připsali důležitý počet bodů, takže se hráč zavázal nám. Takže ne vždy je ztráta 1000 bodů zásadní, ale stojí to hodně a investici si musíte obhájit.

Co se také projevilo jako velmi dobrá strategie, je hledání "volných hráčů". To jsou tací, kteří i po pár týdnech sezóny stále nemají zájemce o své služby. Zpravidla mají maximálně 2 hvězdy, jenže OVR může být klidně 67 při potenciálu A- a odolnosti (durability) A-. Takového hráče získáte snadno a zároveň získáte hráče, který se může ve slabších programu stát hvězdou, jenž tým dovede k úspěšné sezóně.

#### Závazky
Závazky jsou, že hráč se (zatím) pevně rozhodne, že chce být součástí nějaké univerzity. Vždy si však vybírá jenom z těch, které mu nabídly stipendium a soutěž vyhrává univerzita, která u daného rekruta nasbírala nejvíce náborových bodů. Období, kdy se rekruti zavážou, je však různé. Nejdříve jsem viděl rekruta se zavázat tuším ve 4. týdnu, nejpozději je to hned po 13. týdnu (rivalitní zápasy). Pokud se rekrut nezaváže po 13. herním týdnu, udělá to až v den podpisů (Signing day).

Stipendia doporučuji nabízet až ve chvíli, kdy se chce student zavázat (Will commit soon, případně Estabilishing). A to i pokud mu už stipendium nabídly jiné programy. Zde pořád stačí využívat jen ostatní akce a udržovat odstup. Za stipendium + target se dá získat i 500 bodů, takže i taková ztráta je v klidu. Tato strategie mi umožňuje udržet kontrolu nad hráči, které mohu získat. Není nic horšího, než nabídnout stipendium 5 CB, když potřebujete jen 2 (ale cílíte vždy na více) a nakonec získáte všech 5. Je jasné, že někteří pak nebudou hrát, což není dobře. Hlavně Vám ale vezmou místo pro jiné hráče, v jedné sezóně je možné získat max 25 hráčů.

### Depth chart
Před každým zápasem je dobré zkontrolovat aktuální soupisku a případně upravit/posunout hráče nahoru/dolů, aby získali adekvání zápasové zatížení. Herní vytížení je důležité pro spokojenost hráče, ale i pro zlepšení na konci sezóny. Může být dobré pořadí hráčů zablokovat, aby se případně jen posunuli hráči nahoru, pokud před nimi někdo vypadne kvůli zranění. Pokud se pozice nezablokují, může hra hráče přeuspořádat dle svého.

Pro jednotlivé zápasy je ještě možné zvolit, jestli mají hrát spíše starteři nebo jestli má docházet k rotaci, třeba i hloubkové. To se řeší v rámci Game plan.

### Trénink (Practice)
Zde se nastavují primární zaměření v závislosti na soupeři. Tzn. na co se má zaměřit útok, aby využil soupeřových slabin a na co se má zaměřit obrana, aby eliminovala soupeřovy silné stránky ofenzivy. Po nastavení zaměření je možné opět vyzkoušet 8 her pro daný týden.

Volby her jsou:
 * **Scrimige** - hrají proti sobě obrana a útok vlastní univerzity, jen se zkouší co a jak
 * **Offense** - testují se vlastní ofenzivní hry proti předpokládané obraně soupeře (zde je vhodné otestovat oblíbené hry z playbooku)
 * **Deffense** - testují se vlastní obranné hry proti předpokládaným útokům soupeře (testování stejně jako u offense)

### Simulace hry
Záleží na tom, v čem jsme dobří (jako uživatel) a v čem jsou dobří naši trenéři (HC/OC/DC). Je možné simulovat rovnou celý zápas, takže se dozvíme výsledek, nebo hrát opravdu každou hru v ofenzivě i defenzivě. Osobně si v defenzivě "jistý v kramflecích", proto bych volání obranných her nechal na DC (což ale vyžaduje jeho silné hodnocení ve volání her). V tomto případě je nejvhodnější zvolit simulaci do další změny držení míče. Ofenzivní hry bych si možná troufl volat a proto bych se nebál "ignorovat" HC i OC.

Tito dva tak mohou mít horší úroveň volání her, ale dají se lépe využít na nábor.

### CCG, Play-off a Bowly
Po regulérní sezóně, která má 12 zápasů přichází na řadu postupně Konferenční finále (CCG), Play-off (nově 12členné, takže na 4 kola) a do toho jsou vloženy Bowly pro ostatní týmy. Ti co vypadnou z PO už se pak žádného Bowlu neúčastní. Výsledkem je, že jsou pak známí vítězové konferencí (někteří postoupí do PO), vítězové Bowlů a národní šampion. Všechny utkání mají zpravidla pozitivní vliv na prestiž a některá další hodnocení.

## Offseason

### Zlepšení trenérů (Coach Updates)
Možnost vylepšit trenéry, rozdat jim nějaké body... pokud postoupí o level výš, je možné body nějak rozdělit. Za každý level je možné přidat i nějaký "odznak", tedy speciální vlastnost, která pomáhá. Může to být třeba jedna tréninková session navíc (což v předsezónních trénincích dělá dohromady 4 akce).

U HC doporučuji jít primárně po vylepšení Tréninku (training) a Náboru (recruiting), u OC/DC je to ofenzíva/defenzíva a pak trénink. Až později se vyplatí investovat do dalších věcí, ale u HC je ideální dostat se na staty 1/1/10/10, naproti tomu u koordinátorů 10/1 resp. 1/10 a pak 10/cokoli.

### Výměny trenérů (Coach carousel)
Tady je možné vyhodit trenéra, pokud se nám třeba nelíbí, nebo najmout trenéra za odchozího (důchod, pozice jinde). Ale po povedené sezóně se prakticky nedá zbavit HC (u školní varianty hry). Naopak je snadné přijít o OC/DC po úspěšné sezóně, protože je odláká jiný program (buď na stejnou pozici, nebo rovnou jako HC). Pak je třeba najmout nového trenéra. 

U HC je to jednoduché, s novým HC můžeme najmout kohokoli a klidně celý program přebudovat, ale pokud je HC jasně daný, je třeba se mu přizpůsobit. Pokud však vím, že HC chci za rok vyhodit, mohu již o rok dříve nabrat kvalitní koordinátory, kteří budou odpovídat mé budoucí filosofii. Sice tím pravděpodobně odepíšu následující sezónu, ale můžu si připravit půdu pro nového HC. Ovšem může se stát i to, že nového HC za rok nenajmu, neboť nebude k mání ta správná osoba.

Na vše je třeba dát pozor, ale pokud mám potvrzeného HC, měl bych zbytek přizpůsobit jemu. Tedy HC určuje plán hry na obou stranách míče. Pokud je např. v útoku Spread / Run First, je prioritou přivést OC, který také vyznává Spread. Ale OC může být klidně Pass First nebo Balanced, s tím už není takový problém. V defenzívě platí to samé.

### Odchody hráčů (Players Leaving)
Primárně seznam hráčů, kteří jdou pryč z univerzity. Hlavně ti co dostudovali nebo odešli na draft. Tuto část prakticky není možné ovlivnit (zatím - časem by měla přibýt možnost rozmluvit hráči odchod na Draft, pokud má ještě nějaké roky na univrzitě před sebou).

### Přestupy (Players Transferring)
Tady se můžeme podívat, kteří hráči od nás chtějí odejít. To je ostatně indikováno už v průběhu sezóny smajlíkem u jejich jména. Takže pokud chce nějaký hráč odejít, víme o tom zpravidla s předstihem. Zde máme pouze možnost mu to zkusit rozmluvit. Ovšem to je zpravidla za cenu výrazných slibů. Takže pokud se nejedná o Jr hráče, který se má příští rok stát starterem, je to slibování poněkud zbytečné. Zvláště, když o jeho nespokojenosti vím dopředu. Mám dost času na to zareagovat a pokusit se rekrutovat nějakého mladíka na danou pozici (popř. se o to mohu pokusit z Transfer portalu).

### Pro Draft
Tady se dozvíme, kteří hráči odešli na draft a byli draftování, kdo šel do síně slávy atd. Jen taková souhrnná část, lze proletět rychle.

### Posezónní nábor (Offseason recruiting)
Dokončení rekrutování na cíle, které se ještě nezavázaly. Popř. podávání nabídek na hráče z Transfer portálu. Tento nábor má 3 části, v každé je možné vyplýtvat náborové akce, nabídnout stipendia nebo rovnou peníze NIL. Mnohem důležitější jsou zde souboje o hráče z Transfer portalu, než-li o rekruty (ti už bývají buďto zavázáni, nebo máte zpravidla náskok a oni jen čekají).

Hráči kteří jsou na Transfer portal zpravidla nevyžadují NIL, ale naopak herní vytížení. Toho se dá dobře využít na slabších školách, kde budou takovíto hráči rovnou starteři. NIL pak pomůže získat výhodu, ale nepomůže smazat velkou ztrátu, na to je třeba si dát pozor.

V první části dochází zpravidla jen k prvotnímu kontaktu, na nejlepší hráče je třeba vyplýtvat nejlepší akce, ale nenabízet ani peníze ani stipendia. S tím je lepší počkat na další dvě části. Hráči zpravidla vyčkávají co všechno se bude dít. Získat v této fázi 1800 až 2000 bodů (bez initial contact) je super pro další vyjednávání. Hlavní zájemci mívají kolem 2700 bodů.

Ve druhé fázi se de facto rozhoduje, na které cíle se zaměřit. Pokud po první fázi ztrácíte více než 1000 bodů, je třeba zvážit zda vynaložené prostředky za daného hráče stojí. Pokud už ztrácíte 1500 a více bodů, je lepší to vzdát. Do tisíce bodů se dá poměrně solidně bojovat i bez peněz, ty je možné nabídnout až na konci. Nad tisíc bodů je třeba už i v této fázi nějaké finance pustit. Proto je nutné zvážit, o které hráče opravdu stojíte. Zároveň může být problém se stipendii, protože někteří rekruti už se mohli univerzitě zavázat a na ostatní (lepší?) hráče nezbývá dostatek prostoru.

Ve třetí části už je jasné, které hráče máme šanci získat a které ne. Pokud kdekoli ztrácíme více než 1000 bodů, je lepší to zabalit. Cena za zisk takového hráče by byla neskutečně vysoká. Ušetřené peníze (NIL) pak je možné na konci investovat do zázemí, což se může pozitivně projevit hned v dalším náboru. Někdy je prostě lepší ušetřit 1,5M na jednom hráči, výměnou za to, že zlepším image školy vůči všem.

Zároveň je důležité nabídnout stipendium všem hráčům, o které stojíme, tito se totiž zavážou pouze tam, kde získali nabídku stipendia. Tedy vždy si vyberou tým s nejvíce body, který jim stipendium nabídl. Nestačí tedy pouze vyhrát bitvu s ostatními univerzitami, ale je třeba i nabídnout stipendium.

### Den podpisů (Signing Day)
Nejpozději v tento den se zavážou i poslední rekruti, kteří neměli angažmá pro příští sezónu. Také je možné si prohlédnout celou skupinu rekrutovanýách hráčů či porovnat se s ostatními programy.

### Vylepšení školy (School updates)
Zde je možnost provést případné vylepšení, které však stojí peníze. Ty je zapotřebí nejdříve získat (výkony v zápasech, zisk boosterů) nebo ušetřit na NIL. Pak je možné je investovat do vylepšení zázemí školy. Čím více peněz ušetříte/získáte tím více pak můžete utratit. Zároveň je třeba finance postupně vkládat do zázemí, nikoli pouze do NIL, protože i zázemí bude chátrat a je třeba se o něj starat. Postupně se vyplatí investovat do všech kategorií, ale ideální je začít s tréninkovými prostory (Facilities), které zvyšují zlepšování hráčů a pak Vzdělání (Academics), které ovlivňuje zlepšování trenérů, ale zároveň i nábor (někteří hráči nehledí na čas, ale na získané vzdělání).

Zároveň se v této části "dopuje" NIL na další sezónu. Peníze co tam máte Vám zůstanou, ale nezvyšují se automaticky, je třeba je vždy doplnit ze získaných financí. I proto je vhodné NIL šetřit, protože když utratíte za sezónu málo, nemusíte pak nic doplňovat a finance je možné utratit za vylepšení zázemí školy.

Co se týče prestiže, tu není možné nijak upravovat manuálně (dopovat penězi), ta se mění automaticky se získanými body. Pokud vyhrajete více zápasů, než je předsezónní predikce a vyhrajete třeba konferenční titul, je vysoká pravděpodobnost, že získané body umožní posílit prestiž výrazně. Např. hned v první sezóně za Jacksonville se mi podařilo uhrát bilanci 8-2 (8-1 v konferenci), vyhrát titul a prohrát v prvním kole play-off (kam jsme se po haluzi dostali jako poslední tým celkově - vše jen díky jedinému hráči, vygenerovanému DL, který se stal DPOY, AAT a na draftu šel jako dvojka celkově). Díky těmto výsledkům jsem získal tolik bodů, že prestiž poskočila během jediné sezóny z 1 hvězdy rovnou na 4.

### Změny v odměnách (Booster updates)
Informace o úpravách v odměnách, tedy v nových výzvách, které je nutno splnit pro zisk odměn (peněz). Vše závisí na změnách prestiže, pokud se přestává dařit, podporovatelé odcházejí nebo se stávají neaktivními. Naopak pokud se daří, mohou přijít noví. Čím lepší podporovatel (diamantový je nejlepší) tím více peněz, zpravidla ale tím těžší výzva. Např. v první sezóně za Jacksonville mi chyběly jen 3 běhové TD (s podprůměrným běhovým útokem), abych získal 5,5M! Po sezóně mě opustil OC (Spread/Pass first), tak jsem nabral OC, který bude lépe pasovat v HC (Balanced/Balanced). Na konci přišel druhý diamantový podporovatel, který pro změnu přidal podmínku pro pasové TD.

### Změny v konferencích (Conference Realignment)
Je možné kouknout na změny prestiže v konferencích, na přesuny týmů atd. (závisí na výchozím nastavení celé hry - pokud máte realignment vypnutý, nestane se nic). Ale můžete tady vidět, jak jste se posunuli v rámci konference. V již zmíněné hře za Jacksonville jsem prestiže zlepšil natolik, že jsem se hned po první sezóně vyšvihl ze dna konference na úplný vrchol (sakra to byl nepříjemný závazek, když jsem přišel o nejlepšího hráče).

Na závěr se ukončí uplynulá sezóna a začne ta nová.
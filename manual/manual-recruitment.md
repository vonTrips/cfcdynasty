# College Dynasty - Návod na rekrutování
Jedná se o hrubý překlad návodu, který sepsal uživatel Swiss na Steamu - je to dokonce na Steamu jako oficiální návod, stejně jako na Discordu. Jen jsou některé věci drobně upraveny, popř. dále mírně doplněny.

## Krok 1: Určete potřeby náboru.

Začněte vyhodnocením, kolik máte v týmu seniorů a kolik jich bude v týmu po ukončení studia. Základní počty pro každou pozici jsou:
 * QB - 3
 * RB - 4
 * WR - 8
 * TE - 4
 * OL - 12
 * K - 2
 * DL - 8
 * LB - 8
 * CB - 7
 * S - 5

Celkový počet je 61. Je možné mít jich více, ale toto by měl být minimální počet hráčů v týmu. Čím více jich budete mít, tím méně herního času budou mít ti, kteří jsou na konci sestavy, a tím větší je riziko, že váš program opustí.

### Hráči navíc - Redshirts

Rád bych měl na každé pozici jednoho hráče navíc, který by byl na začátku sezóny "vyřazen z kádru". V podstatě vezmete nováčka, dáte ho do posilovny a on celý rok studuje film. Nebude na aktivní soupisce. Tím se dosáhne několika věcí. Zaprvé, pokud máte přebytek nováčků, vyrovnáte tím jejich počet. Zadruhé, na začátku příštího roku budete mít na dané pozici zaručeně nováčka. Za třetí, a to je možná nejdůležitější, tento RSFR (redshirt freshman) bude mít rok navíc na rozvoj. Z tohoto důvodu se snažím umístit prváka s velmi dobrým potenciálem...B nebo lepším, abychom získali co největší hodnotu z roku tréninku navíc.

## Krok 2: Důsledná analýza

Jakmile máte seznam hráčů, které potřebujete, musíte určit, na které hráče se zaměřit. K dispozici máte různé filtry. Zde je uvedeno, jak přistupuji k počátečnímu procesu cílení.

Postupně na jednu pozici. Pokud pro příští sezónu nepotřebujete QB, zrušte jejich výběr z hlavního seznamu. Čím více hráčů na začátku procesu vyřadíte, tím snazší bude celý proces.

Řekněme, že QB potřebujete. Může jich být ve výběru třeba sto. Použijte rozevírací pole a vyberte POUZE ty QB, u kterých jste v první pětce zájmu. MŮŽE se vám podařit získat jednoho hráče, i když jste mimo počáteční první pětku, ale pokud ano, bude vás to stát VELKÉ množství prostředků na nábor a to za to prostě nestojí (většinou).

Když se podíváte na seznam QB (nebo na jakoukoli jinou pozici), vyberte "TARTGET" pouze u těch QB (nebo jiných), u kterých máte pocit, že máte reálnou šanci je získat. Proto začínám s hráči, kteří mají můj program již v první pětce zájmu.

Máte možnost zaměřit se na 40 hráčů. Pokud chcete získat celkem deset, je to snadná matematika. Zaměřte se na čtyři hráče na každou pozici, kterou je třeba obsadit. Pokud potřebujete tři OL, pak se zaměřte na 12 hráčů. Dále v horní části obrazovky vyberte možnost "TARGETS ONLY" ..... Tím bude váš seznam od této chvíle maximálně 40 hráčů.

V TUTO CHVÍLI NENABÍZEJTE STIPENDIA, JE PŘÍLIŠ BRZY.

### Cíle (TARGET)
Zacílení je možné kdykoli zrušit, např. zjistíte, že hráč má velmi špatný potenciál a k tomu i špatnou odolonost (Durability), tak je lepší jej vyřadit ze zacílení a zaměřit se na ostatní hráče v užším seznamu.

Naopak po vyřazení jednoho můžete zkusit zacílit na jiného hráče, i když je tam třeba větší rozdíl. Důležité je totiž to, že pokud máte hráče v seznamu (Target), automaticky u něj přibývají náborové body. Za každý týden náboru (a samozřejmě i regulérní sezóny) získáte u TARGET hráče minimálně 100 bodů, za celé období to tak může být 1500 bodů jen proto, že jste jej označili jako cíl.

### Bez zájmu / minimální zájem
Co je rozhodně zajímavé, to jsou hráči bez zájmu. Tedy hráči, o které se nezajímá žádná univerzita. Stačí filtrovat "Poinst ahead" nebo "Closest battle" a tito hráči budou za posledních stranách seznamu. Poznáte je tak, že v kolonce zájmu není znak žádné univerzity. Popř. je tam jenom jeden.

Tyto hráče je pak snadné získat jenom tím, že na ně zacílíte (target) a není třeba u nich utrácet další náborové akce, které tak můžete investovat do důležitějších cílů. U hráčů s minimálním zájmem je to podobné, pokud tam jsou jen 2 nebo 3 univerzity a ztráta je pouze pár stovek, stačí hráče zacílit a ztrátu v náboru snadno doženete.

Toto však vyžaduje ruční procházení seznamu s rekruty, zatím se nedají nijak vyfiltrovat, bohužel.

## Krok 3: Skauting

Seřaďte hráče podle pořadí a použijte deset bodů skautingu, jeden na osobu. Každý týden utrácejte jeden skautský bod za každý cíl a skautujte hráče s nejnižším skautským hodnocením. Tímto způsobem získáte nejvíce informací a nejrychleji.

Když se vám podaří získat informace o potenciálu a odolnosti (durability), použijte je jako rozhodující faktory pro své potenciální rekruty. 61-A je mnohem lepší než 66-D. Odolnost je také důležitá. Nedotknu se hráče s hodnocením "F". Nebude vám moc platný, když bude postávat v civilu u postranní čáry a chodit o berlích.

Jsou tu nějaké nehmotné vlastnosti... pokud chcete mít běhající tým, hledejte OL, kteří mají dobré schopnosti blokování běhu. To samé platí pro obranu. Do vašeho programu se nehodí silný hráč typu muž proti muži, pokud hrajete zónovou obranu. To vše se vrací k určení konkrétního typu hráče, kterého ve svém programu chcete. Můžete říct....OL, 60 plus pro základ, B nebo lepší pro potenciál, se zkušenostmi v blokování běhu a odolný.

## Krok 4: Akce v prvním týdnu

Jakmile máte vybraných všech čtyřicet cílů (hráčů), můžete se rozhodnout, jak naložíte s náborovými akcemi. Na každou pozici dělám JEDNU akci a střídám je, přičemž začínám návštěvami v kempu. Tím získáte příjemný počáteční nárůst náborových bodů. Pak použijte akce HC, OC a DC, postupně na ty nováčky, o kterých si myslíte, že by vašemu programu nejlépe prospěli.

## Krok 5: Filtrování priorit akcí

Použijte rozevírací seznam a vyhledejte rekruty, kteří chtějí POUZE váš program. Nemusíte na ně vynakládat akci, ale poznamenejte si, kolik jich je. Nechal bych si je cílené, jako pojistku proti ztrátě jiných, lepších kandidátů (tzn. budou v seznamu cílů - Target, ale nebudete na ně plýtvat náborové akce).

Druhým filtrem jsou "bodově nejbližší". Ten používám nejčastěji. Pokud máte náskok 800 bodů, nemusíte utrácet NEJLEPŠÍ náboje, abyste se dostali o tisíc bodů napřed. Nechte tohoto hráče týden na pokoji a sledujte, zda se náskok 800 bodů zmenšuje a případně o kolik. Potřebujete své náboje, abyste se stali konkurenceschopnějšími vůči ostatním univerzitám tam, kde probíhá skutečná bitva. Pokud vidíte, že ztrácíte několik set bodů, zvažte vyřazení tohoto hráče ze seznamu cílů. Bez utracení VASTNÍCH bodů ho nezískáte. 

Také v tomto okamžiku šetřete své NIL peníze. Ty použijte velmi, VELMI pozdě v celém procesu... třeba těsně předtím, než se rekrut zaváže. To by vám mohlo dát malou vzpruhu ke skoku vpřed, těsně před podpisem.

## Návštěvy
Návštěvy jsou důležitou součástí náboru, potenciálním rekrutům ukazujete svou univerzitu (zázemí, stadion, tým) tím, že je pozvete na vybraný zápas.

Naplánujte si návštěvy, až dostanete podnět od každého hráče. Čím dříve, tím lépe. Použijte informační obrazovku každého rekruta, abyste zjistili, jaké jsou jeho potřeby. Někteří budou chtít MAXIMÁLNÍ peníze, a pokud jste bez peněz, nebude to fungovat.

Návštěvy se však nabízejí pouze v preseason, v sezóně už potom tyto návštěvy (maximálně jich může být 40) probíhají. Některé návštěvy je možné naplánovat už v průběhu jarních/letních tréninků, jiní hráči vám umožní je pozvat až v průběhu sezóny. Proto je důležité zvát hráče vždy na nejbližší možné utkání.

Zároveň je třeba dobře vybírat, které hráče pozvete na které utkání. Čím silnější soupeř, tím více bodů získáte. Čím zajímavější soupeř (např. rivalita), tím více bodů. A tak dále. Rozhodují í výsledky, ale vždy je třeba si dát pozor na počet rekrutů, kteří k Vám přijdou na daný zápas!

### Počet návštěv najednou dle pozic
U každé pozice je totiž možné provést jen omezený počet návštěv najednou (na jeden zápas), aby nedošlo k penalizaci. Pokud překročíte níže uvedený počet (u každé pozice samostatně), dojde k penalizaci u náborových bodů:
 * QB: 1
 * RB: 2
 * WR: 3
 * TE: 2
 * OL: 4
 * K: 1
 * DL: 3
 * LB: 3
 * CB: 3
 * S: 3

## Potenciál
Při rekrutování je dobré se zaměřit na hráče co mají potenciál B a lepší. U transferů (hlavně starších hráčů) už to může být i horší potenciál, neboť ten se v průběhu času zhoršuje s tím, jak je postupně naplňován. Hráč 90+ (rating) už může mít potenciál F, což značí, že je téměř na vrcholu svých dovedností.

## Stipendia
Stipendium je náborová akce. S nabídkou stipendia čekám, dokud se nenabídne tým soupeře. Tímto způsobem mohu získat lepší představu o tom, jak si u daného hráče stojím.

Pokud se během jednoho týdne dostanu z +200 na -300, je lepší nejdříve nabídnout stipendium / vyplýtvat náborové akce, než se zbláznit a nabídnout NIL peníze. Není nic horšího, než nabídnout hráči hromadu peněz a o šest týdnů později být v plusu o tisíc náborových bodů.

## Náborová strategie
Mým cílem je nahradit starší hráče a pak mít na každé pozici ideálně jednoho hráče navíc, kterého můžu převychovat. Je tu 8 pozic, na kterých mi záleží. U kickerů je možné vynechat tuto pozici z náboru, příchozí walk-on mnohdy stačí a náborové akce se dají využít lépe.

Není od věci kombinovat pozice (např CB/S nebo DL/LB) a hráče mezi nimi vždy před sezónou prohodit. Tím se vyrovnají ročníkové skupiny, aby byl ideálně stejný počet hráčů. Někdy je to tak, že jsou v náboru pouze CB, kterých tak naberu více a před sezónou udělám ze dvou z nich Safety.

Také je dobré mít na každé pozici jednoho hráče navíc, kterého označíte jako RS (RedShirt). Ten u Vás pravděpodobně stráví o jeden rok víc, ale pokud má dobrý potenciál, rok v tréninku navíc z něj udělá lepšího hráče, než jej pošlete na hřiště.

Dobré je omezit nábor hráčů na hráče s potenciálem A, B nebo C, a to i v případě, že se vám nedaří doplnit soupisku a nahradit odcházející seniory. Walk on bude lepší než většina hráčů D nebo F, které můžete nabrat. Možné ne na začátku, ale časem asi ano. Popř. jej časem bude možné cutnout a místo něj nabrat lepšího nováčka.

Prostě je zbytečné plýtvat náborovými akcemi na hráče s potenciálem E nebo F, leda byste byli hodně vpředu bez nutnosti tyto akce utrácet. Popř. pokud jsou to hráči s vyšším ratingem, např. přichází z JUCO, pak je možné snažit se nabrat i tyto hráče s nižším potenciálem. (Např. je rozdíl mít HS kickera 74 E+ a HS kickera 65 A - ten rozdíl se maže těžko).

## Rekapitulace
Rekapitulace stručně v bodech (možná bude časem přibývat)

 1. Filtrujte podle jednotlivých potřebných pozic a najděte hráče, u kterých jste již v první pětce.
 2. Zaměřte se na 40 hráčů na základě potřeb
 3. Proveďte průzkum a pozvání na kemp. Používám "žebříček", protože nakonec budou všichni pozváni na kempy (máte 40 cílů a 4x10 pozvánek na kempy).
 4. Podle pozice sestavte žebříček svých cílů. Vynakládejte prostředky na základě kritických potřeb. Čím těsnější souboj a zároveň i čím větší je váš zájem, tím větší náboj byste měli pro daný týden použít.
 5. Každý týden, kdy postoupíte, se podívejte na ty bitvy, které jste prohráli nebo prohrajete, a rozhodněte se, zda má cenu pokračovat v boji, nebo snížit ztráty a získané zdroje použít na to, abyste se pokusili získat jiného rekruta.
 6. Ve čtvrtém týdnu nebo při verbování se obvykle dostávám na 20 cílů pro deset volných míst nebo tak nějak.
 7. Nenabízejte čtyři stipendia na dvě volná místa. Takhle získáte dvacet OL. Můžete nabízet brzy, ale sledujte, kolik jste jich nabídli, podle pozic.
 8. Po skončení všech náborů se podívejte, co jste získali a co jste naopak nezískali. V ideálním světě jste nemuseli utratit žádné peníze NIL. Peníze NIL si nechávám na přestupový portál. Často se jedná o chamtivé hráče, již zkušené, kteří chtějí dostat zaplaceno. Pokud máte nějaké peníze stranou, můžete získat zajímavé vylepšení na pozici startera.

Také... pokud máte více cílů než volných míst, škrtejte ze spodní části tohoto seznamu. Pokud potřebujete dva a jste těžkými favority pro dva, pak si tyto dva ponechte a cíle z ostatních odstraňte. Nedělal bych to, dokud si nebudete zcela jisti, že vás nemůže předstihnout jiný tým. Já mám každopádně rád tři terče na dvě volná místa, abych se mohl zásobit RS. Problém je jen u potenciálu. Pokud vedete u dvou hráčů co mají oba potenciál D a horší, není od věci investovat kapitál do hráčů s lepším potenciálem a tyto si ponechat jen jako zálohu.
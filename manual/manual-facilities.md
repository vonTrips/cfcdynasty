# Vylepšení zázemí

Zjistil jsem, že nejjednodušší cestou k úspěchu při budování školy je zaměřit se na zařízení. Dokonce bych doporučoval nevyčerpat maximum peněz na NIL, abyste zvýšili hodnocení zařízení.

1. Pomáhá to při náboru.
2. Pomáhá to zlepšit hráče, které získáte.
3. Pomáhá to také při skautingu pro návštěvy kempů, ale nemyslím si, že je to až tak důležité.

Dále záleží na tom, jak je postavený váš trenér nebo jak moc chápete herní systémy. 

Akademie vám dá větší šanci získat lepší sponzory a jako každá kategorie pomáhá s náborem hráčů. Také pomůže vašim trenérům rychle růst, což vás pak dělá lepšími ve hře. 

Pokud máte problémy s náborem dobrých hráčů, možná by bylo dobré začít posilovat svůj Campus Lifestyle.

1. Pomáhá to získat rekruty, kteří chtějí dobrý campus lifestyle.
2. Pomáhá všem rekrutům při návštěvách, protože vám dává 300 bodů.  To by vám mohlo získat hráče právě tam.

Pokud jste dobří v náboru, mohli byste se přesunout na stadion, protože vám to pomůže vyhrát domácí zápasy.  Výrazná výhoda domácích zápasů může průměrnou sezónu extrémně rychle proměnit v dobrou.  Což by pak mohlo vést k tomu, že se prestiž vaší školy začne nabalovat jako sněhová koule. Pomáhá to také při náboru. 
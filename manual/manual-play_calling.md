# Zlepšování trenérů a efektní volba her

## Trenéři
Nejprve zlepšování trenérů. S každou vyšší úrovní můžete přidat něco buď do útoku, obrany, tréninku, nebo náboru a na určitých úrovních můžete přidat vlastnosti. To se děje u každého trenéra. 

U vlastností do značné míry záleží na schématu, ale vždy je důležité zaměřit se na tréninkové odznaky a maximalizovat nábor, který odpovídá vaší obraně, a pak získat zvýšení dovedností, které odpovídají vašemu stylu hry. 

## Play calling (trenéři)
Vždy mějte OC a DC, kteří odpovídají preferencím vašeho hlavního trenéra. Hodně to zvýší váš herní styl. 13 je maximální efektivní úroveň playcallingu.

Efektivní volání her je důležité bez ohledu na to, zda voláte hry osobně, nebo volání her simulujete. 

Pokud máte 1 z útoku nebo obrany a váš koordinátor má 10, bude to vždy 13, což znamená, že není nutné vylepšovat obě úrovně dovedností, když jste v bodě, kdy máte koordinátory s maximální úrovní ve svém oboru. Navíc to znamená, že v oblasti, na kterou se specializujete, potřebuje koordinátor také méně úrovní. Tento koordinátor může mít více bodů vložených do náboru a výcviku. 

Nejsem si jistý, ale myslím si, že efektivní volba her je dána úrovní koordinátora a trenéra + přídavky ze sladění preferencí. 

## Play calling (vlastní)
Toto je ideální volba, pokud hrajete z pozice HC. Z pozice školy je to poněkud zvláštní, protože všechny 4 důležité pozice (HC, OC, DC a AD) jsou cizí osoby a přesto můžete volat hry (to by asi mělo být omezeno). Každopádně je ideální stav pokud rozumíte oběma stranám míče a jste schopni volat hry sami, popř. si vybrat z koordinátorem navržených her.

Správným play-callingem je možné otočit některé těšné utkání ve vlastní prospěch. Např. jsem zkusil nechat hru odsimulovat utkání mého Jacksonville proti Horníkům z El Pasa - z pěti simulací bylo 5 proher, všechny maximálně o FG. Pak jsem zkusil 5x volat vlastní ofenzivní hry (defenzivě rozumím ještě méně) a prohrál jsem jen jednou, když se mi zranil QB. Ostatní čtyři utkání jsem vyhrál, i když každé jen o jedno držení míče (+1, +3, +3 a +7). Z toho jasně vyplývá, že je lepší naučit se systémy, naučit se číst soupeře a volat vlastní hry.
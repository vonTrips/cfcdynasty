# CFCDynasty

Český repositář s návody a informacemi ke hře Football Coach: College Dynasty (Steam). Skládá se z těchto materiálů:

 * **Adresář manual** - md soubory s manuály (slouží primárně jako záloha)
 * **Adresář universe** - soubory s nastavením herního světa ke stažení (FBS, FCS a D2)
 * **About.md** - taková malá recenze/popis hry
 * **Wiki** - Wiki k repositáři, kde jsou právě i interaktivní manuály (je snazší na údržbu)

 ## Zdroje
 Základními zdroji je moje vlastní hraní a pak to co se dalo sehnat na Discordu hry, v diskuzi ke hře na Steam.